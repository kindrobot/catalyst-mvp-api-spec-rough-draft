# Catalyst MVP API Spec Rough Draft

A strawdog proposal for an OpenAPI specification for the Catalyst API (MVP).

## Best way to view

Open [openapi.yaml](./openapi.yaml) on GitLab to see the documentation in a human readable format.

## License

MDGPL